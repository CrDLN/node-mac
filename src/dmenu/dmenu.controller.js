const express = require('express');
const DmenuController = express.Router();
const DMenu = require('./dmenu.model');
const DMenuService = require('./dmenu.service');


DmenuController.get('/', async (req,res, next)=>{

    try {
        const dmenus = await DMenuService.find(req.query.extended);
        res.json(dmenus);
    } catch (error) {
        next(error);
    }
});


DmenuController.get('/:id', async (req,res,next)=>{

    try {
        const {id} = req.params
        const dmenu = await DMenuService.findOne(id);
        res.json(dmenu);
    } catch (error) {
        next(error)
    }
    
 });

 DmenuController.post('/', async(req,res,next)=>{
    try {
        const {weight,kcal,type,idmenu} = req.body;
        const dmenuCreated = await DMenuService.create({weight,kcal,type,idmenu});
        res.status(201).json(dmenuCreated);
    } catch (error) {
        next(error);
    }
 })
 
 DmenuController.put('/:id', async(req,res,next)=>{
    
    try {
        const {id} = req.params;
        const {weight,kcal,type} = req.body;
        const dmenuReplace = await DMenuService.replace(id,{weight,kcal,type});
        
        res.json(dmenuReplace);
    } catch (error) {
        next(error);
    }

 })

 DmenuController.delete('/:id', async(req,res,next)=>{

    try {
        const {id} = req.params;
        await DMenuService.delete(id);
        res.status(204).send();

    } catch (error) {
        next(error)
    }
 })

module.exports = DmenuController;