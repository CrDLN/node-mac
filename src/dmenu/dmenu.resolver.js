const DMenu = require("./dmenu.model");

class DMenuResolver {
    static async findByIdMenu(id){
            return DMenu.find({idmenu:id});
    }
}

module.exports = DMenuResolver;