const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const dmenu = {
        weight:{type:Number},
        kcal:{type:Number},
        type:{type:String},
        idmenu:{type: mongoose.Types.ObjectId, ref:'Menu', required:true}
}

const dmenuSchema = new Schema(dmenu, { timestamps: true })

const DMenu = mongoose.model('DMenu', dmenuSchema);

module.exports = DMenu;
