const MenuResolver = require("../menu/menu.resolver");
const StatusError = require("../_shared/error/status.error");
const DMenu = require("./dmenu.model");


class DMenuService {

    static find(extended){

        if(extended==='true'){
            return DMenu.find().populate('idmenu');
        }

        return DMenu.find();
    }

    static async findOne(id){
        const dmenu = await DMenu.findById(id);

        if(dmenu){
            return dmenu;
        }
        throw new StatusError(404,`The dmenu with id <${id}> was not found`);
    }

    static async create(dmenu){
        
        //comprobamos si existe ese menu en relacion
        await MenuResolver.menuExistsById(dmenu.idmenu);

        return DMenu.create(dmenu)
    }

    static async replace(id,dmenu){
        const updated = await DMenu.findByIdAndUpdate(id,dmenu);

        if(updated){
            return updated;
        }

        throw new StatusError(404,`The dmenu with id <${id}> was not found`)
    }

    static async delete(id){

        const dmenu = await DMenu.findById(id);

        if(dmenu){
            return DMenu.findByIdAndRemove(id);
        }
        throw new StatusError(404,`The dmenu with id <${id}> was not found`)
    }

}

module.exports = DMenuService;