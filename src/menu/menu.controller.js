const express = require('express');
const upload = require('../_shared/middleware/file.middleware');
//lo enrutamos
const MenuController = express.Router();
const MenuService = require('./menu.service');


MenuController.get('/', async (req,res,next)=>{
    try {
        const menus = await MenuService.find();
        res.json(menus);
    } catch (error) {
        next(error);
    }
})

MenuController.get('/:id', async (req,res, next)=>{
    
    try {
        const {id} = req.params;
        const menu = await MenuService.findOne(id);
        
        res.json(menu);

    } catch (error) {
        next(error);
    }
    
});

MenuController.post('/', upload.single('image'), async (req,res,next)=>{
    try {
    const {name,cost,ingredients} = req.body;

    const menuCreated = await MenuService.create({name,cost,ingredients}, req.file);

    res.status(201).json(menuCreated);

    } catch (error) {
        next(error);
    }
    

})

MenuController.put('/:id', async (req,res,next)=>{

    try {
        const {name,cost,ingredients,image} = req.body;
        const { id } = req.params;

        const replaceMenu = MenuService.replace(id,{name,cost,ingredients,image})
        res.json(replaceMenu);

    } catch (error) {
        next(error);
    }
   
})

MenuController.delete('/:id', async (req,res,next)=>{
    try {

        const {id} = req.params;

        await MenuService.delete(id);

        res.status(204).send();

    } catch (error) {
        next(error);
    }
})

module.exports = MenuController;
