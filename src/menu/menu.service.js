const DMenuResolver = require("../dmenu/dmenu.resolver");
const StatusError = require("../_shared/error/status.error");
const Menu = require("./menu.model");


class MenuService {

    static find(){
        return Menu.find();
    }

    static async findOne(id){
        const menu = await Menu.findById(id);

        if(menu){
            return menu;
        }
        throw new StatusError(404,`The menu with id <${id}> was not found`);
    }

    static async create(menu, file){

        if(file){
            return Menu.create({...menu,image: file.path})
        }
        return Menu.create(menu);
    }

    static async replace(id,menu){
        const updated = await Menu.findByIdAndUpdate(id,menu);

        if(updated){
            return updated;
        }

        throw new StatusError(404,`The menu with id <${id}> was not found`)
    }

    static async delete(id){

        const menu = await Menu.findById(id);

        const dmenu = await DMenuResolver.findByIdMenu(id);

        if(dmenu.length){
            throw new StatusError(400,`This menu has linked details menu, you must remove dmenus first`);
        }

        if(menu){
            return Menu.findByIdAndRemove(id);
        }

        throw new StatusError(404,`The menu with id <${id}> was not found`)
    }

}

module.exports = MenuService;