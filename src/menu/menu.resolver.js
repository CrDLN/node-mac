const MenuService = require("./menu.service");

class MenuResolver {
    static async menuExistsById(id){
        try{
            await MenuService.findOne(id);
            return true;
        }catch(error){
            throw error;
        }
        
    }
}

module.exports = MenuResolver;