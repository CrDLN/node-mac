const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const menu = {
        name:{type:String},
        cost:{type:Number},
        ingredients:{type:Number},
        image: {type:String}
}

const menuSchema = new Schema(menu, { timestamps: true })

const Menu = mongoose.model('Menu', menuSchema);

module.exports = Menu;