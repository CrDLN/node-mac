const express = require('express');

const UserController = express.Router();
const UserService = require('./user.service');


UserController.get('/', async (req,res,next)=>{
    try {
        const users = await UserService.find();
        res.json(users);
    } catch (error) {
        next(error);
    }
})

UserController.get('/:id', async (req,res, next)=>{
    
    try {
        const {id} = req.params;
        const user = await UserService.findOne(id);
        
        res.json(user);

    } catch (error) {
        next(error);
    }
    
});

UserController.post('/', async (req,res,next)=>{
    try {
    const {name,email,password,province,age,naffiliate} = req.body;
    const userCreated = await UserService.create({name,email,password,province,age,naffiliate});

    res.status(201).json(userCreated);

    } catch (error) {
        next(error);
    }
    

})

//login
UserController.post('/login', async (req,res,next)=>{
    try {
    const {email,password} = req.body;
    const userLogin = await UserService.login({email,password});

    res.status(201).json(userLogin);

    } catch (error) {
        next(error);
    }
    

})

UserController.put('/:id', async (req,res,next)=>{

    try {
        const {name,province,age} = req.body;
        const { id } = req.params;

        const replaceUser = UserService.replace(id,{name,province,age})
        res.json(replaceUser);

    } catch (error) {
        next(error);
    }
   
})

UserController.delete('/:id', async (req,res,next)=>{
    try {

        const {id} = req.params;

        await UserService.delete(id);

        res.status(204).send();

    } catch (error) {
        next(error);
    }
})

module.exports = UserController;
