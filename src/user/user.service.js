const StatusError = require("../_shared/error/status.error");
const User = require("./user.model");
const bcrypt = require('bcrypt');
const JwtUtils = require("../_shared/utils/jwt.utils");


class UserService {

    static find(){
        return User.find();
    }

    static async findOne(id){
        //lean es para devolver el objeto simplificado de mongoo
        const user = await User.findById(id).lean();

        if(user){
            return user;
        }
        throw new StatusError(404,`The user with id <${id}> was not found`);
    }

    static async create(user){

        const found = await User.findOne({email:user.email});

        if(found){
            throw new StatusError(400,"This user exists with email "+ user.email);
        }

        //cifrar contraseña
        const hashedPassword = await bcrypt.hash(user.password,10);

        return User.create({...user, password: hashedPassword});
    }

    static async login(user){
        
        const found = await User.findOne({email:user.email});

        if(!found){
            throw new StatusError(404,"This user not exists by email");
        }

        const isValidPassword = await bcrypt.compare(user.password, found.password);

        if(!isValidPassword){
            throw new StatusError(403,"Invalid password");
        }

        //generamos el token jwt una vez verificado el password
        const token = JwtUtils.generate(found._id, found.email);

        return {token};
    }

    static async replace(id,user){
        const updated = await User.findByIdAndUpdate(id,user);

        if(updated){
            return updated;
        }

        throw new StatusError(404,`The user with id <${id}> was not found`)
    }

    static async delete(id){

        const user = await User.findById(id);


        if(user){
            return User.findByIdAndRemove(id);
        }

        throw new StatusError(404,`The user with id <${id}> was not found`)
    }

}

module.exports = UserService;