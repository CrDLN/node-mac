const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const user = new Schema({
        name: {type:String},
        email: {type:String, required:true},
        password: {type:String, required:true},
        province: {type:String},
        age: {type:Number},
        naffiliate: {type:Number}
})

const userSchema = new Schema(user, {timestamps:true});

const User = mongoose.model('User', userSchema);

module.exports = User;