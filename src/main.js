const express = require('express');
const mongoose = require('mongoose');
const DmenuController = require('./dmenu/dmenu.controller');
const MenuController = require('./menu/menu.controller');
const UserController = require('./user/user.controller');
const logging = require('./_shared/middleware/logging.middleware');
const secured = require('./_shared/middleware/secured.middleware');
const { defaults } = require('./_shared/utils');
const cloudinary = require('cloudinary').v2;
const app = express();

require('dotenv').config();

const PORT =  defaults(process.env.PORT,3000) ;

cloudinary.config({
    cloud_name:process.env.CLOUD_NAME,
    api_key:process.env.API_KEY,
    api_secret: process.env.API_SECRET
})

//call to controllers
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(logging);

//------------ROUTES--------------//
app.use('/users', UserController);
app.use('/menus', secured, MenuController);
app.use('/dmenus', DmenuController);
//others path havent controller
app.use('*', (req,res)=> res.status(404).json("Path not existing"));

//------------ERROR HANDLER--------------//
app.use((error,req,res,next)=>{

    const exception = {
        status: defaults(error.status,500),
        message: defaults(error.message, 'An unexpected error happened'),
    }
    if(process.env.NODE_ENV !== 'production'){
        exception['callstack'] = error.stack;
    }

    console.error(exception);
    res.status(exception.status).json(exception);
})





mongoose.connect(process.env.MONGO_URI,{ useNewUrlParser:true, useUnifiedTopology:true})
.then(()=>
    app.listen(3000,()=> console.info(`Server is running in http://localhost:${PORT}`))
)
