const jwt = require("jsonwebtoken");

class JwtUtils {

    static generate(id,email){
        return jwt.sign({id:id , email:email},process.env.JWT_SECRET,{expiresIn:'1h'});
    }
    static verify(token){
        return jwt.verify(token, process.env.JWT_SECRET);    
    }
}


module.exports = JwtUtils;