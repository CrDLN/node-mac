const { verify } = require("jsonwebtoken");
const UserService = require("../../user/user.service");
const StatusError = require("../error/status.error");
const JwtUtils = require("../utils/jwt.utils");

const secured = (req,res, next)=> {
    
    try{
        const token = req.headers.authorization;

        if(!token){
            throw new StatusError(403, "Unauthorized");
        }

        const parsedToken = token.replace('Bearer ','');

        const validToken = JwtUtils.verify(parsedToken);

        const user = UserService.findOne(validToken.id);

        req.user = user;

        next();

    }catch(error){
        next(error);
    }

    

};


module.exports = secured;